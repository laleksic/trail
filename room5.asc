int distilleryDestX = 400;
int distilleryDestY = 550;

bool distilleryVisible = false;
function setDistilleryVisible(bool val){
  if(val){
    oIn.Visible = true;
    oDst.Visible = true;
    oMachine.Visible = true;
    oSliders.Visible = true;
    oFurnaceDoor.Visible = true;
    oLowHeat.Visible = true;
    oMidHeat.Visible = true;
    oHighHeat.Visible = true;
    oTap.Visible = true;
    oIn.Clickable = true;
    oSliders.Clickable = true;
    oLowHeat.Clickable = true;
    oMidHeat.Clickable = true;
    oHighHeat.Clickable = true;
    oTap.Clickable = true;
    distilleryVisible = true;    
  }
  else{
    oIn.Visible = false;
    oDst.Visible = false;
    oMachine.Visible = false;
    oSliders.Visible = false;
    oFurnaceDoor.Visible = false;
    oLowHeat.Visible = false;
    oMidHeat.Visible = false;
    oHighHeat.Visible = false;
    oTap.Visible = false;
    oIn.Clickable = false;
    oSliders.Clickable = false;
    oLowHeat.Clickable = false;
    oMidHeat.Clickable = false;
    oHighHeat.Clickable = false;
    oTap.Clickable = false;
    distilleryVisible = false;    
  }
}

function toggleDistilleryVisible(){
  if(distilleryVisible){
    setDistilleryVisible(false);
  }
  else{
    setDistilleryVisible(true);
  }
}

function updateDistilleryPartsPositions(int machineX,  int machineY){
  oMachine.X = machineX;
  oMachine.Y = machineY;
  
  oIn.X = machineX - 40;
  oIn.Y = machineY - 85;  
  oDst.X = machineX + 27;
  oDst.Y = machineY - 82;
  
  oSliders.X = machineX + 24;
  oSliders.Y = machineY + 16;
  
  oFurnaceDoor.X = machineX + 24;
  oFurnaceDoor.Y = machineY + 86;
  
  oLowHeat.X = machineX + 100;
  oLowHeat.Y = machineY - 118;
  
  oMidHeat.X = machineX + 130;
  oMidHeat.Y = machineY - 119;
  
  oHighHeat.X = machineX + 156;
  oHighHeat.Y = machineY - 120;
  
  oTap.X = machineX + 240;
  oTap.Y = machineY -30;
}

function abs(int n){
  if(n>0) return  n;
  else    return -n;
}

bool done = false;
function MoveDistillery(float timing, int X,  int Y){
  int xDiff = X - oMachine.X;
  int yDiff = Y - oMachine.Y;
  
  float intensity = Maths.Sqrt(IntToFloat(xDiff*xDiff+yDiff*yDiff));
  
  float timePerUnit = timing / intensity;
  int timeInFrames = FloatToInt(timePerUnit*40.0, eRoundUp);
  
  int xUnit = FloatToInt(5.0*IntToFloat(xDiff) / intensity);
  int yUnit = FloatToInt(5.0*IntToFloat(yDiff) / intensity);
 
  int i = 0;
  while(i*4<abs(xDiff) || i*4<abs(yDiff)){
    updateDistilleryPartsPositions(oMachine.X + xUnit, oMachine.Y + yUnit);
    i++;
    Wait(1);
  }
  
  //updateDistilleryPartsPositions(X, Y);
  if(oMachine.X!=X && !done){
    done = true;
    MoveDistillery(1.0, X, Y);
  }
}

bool distilleryScenePlayed = false;
function room_AfterFadeIn()
{ 
  if(DOOR_OPEN && TALKED_TO_RUGOBA && !distilleryScenePlayed){
    setDistilleryVisible(true);
    
    MoveDistillery(10.0, distilleryDestX, distilleryDestY);
    
    distilleryScenePlayed = true;
  }
}

function room_Load()
{
  setCurrState(gstNormal);
  LCM_hide();
  RT_setRooms(5);
  int lastRoom = RT_getLastRoom();
  switch(lastRoom){
      case LAKE:{
        player.x = 366;
        player.y = 707;
      break;
      }
  }
  
  RemoveWalkableArea(3);
  RemoveWalkableArea(4);
  RemoveWalkableArea(5);
  
  if(!BLAZH_VISIBLE){
    cBlazh.Transparency = 100;
  }
  
  if(DOOR_OPEN){
    oDoorOpen.Visible = true;
    oDoorOpen.Clickable = true;
    oDoorClosed.Visible = false;
    oDoorClosed.Clickable = false;
    cBlazh.Transparency = 0;
    cBlazh.x = 192;
    cBlazh.y = 590;
    cBlazh.FaceDirection(eDirectionRight);
    //cBlazh.Baseline = 750;
    //RemoveWalkableArea(7);
  }
  else{
    oDoorOpen.Visible = false;
    oDoorOpen.Visible = false;
    oDoorClosed.Visible = true;
    oDoorClosed.Visible = true;
  }
  
  hJournal.Enabled = false;
  if(DOOR_OPEN && !distilleryScenePlayed){
    oJournal.Visible = true;
    hJournal.Enabled = true;
    updateDistilleryPartsPositions(100, -100);
  }
  else if(distilleryScenePlayed){
    oJournal.Visible = true;
    hJournal.Enabled = true;
    updateDistilleryPartsPositions(distilleryDestX, distilleryDestY);
    setDistilleryVisible(true);
  }
  
  if(BERRIES_PICKED_RED){
    oBush.Graphic = 337;
  }else{
    oBush.Graphic = 321;
  }
  
  if(BARREL_BROKEN)    hBarrel.Enabled = false;
  else    hBarrel.Enabled = true;
  
  if(FINISHED){
    RestoreWalkableArea(2);
    RestoreWalkableArea(3);
    RestoreWalkableArea(4);
    RestoreWalkableArea(5);
  }
}

function room_Leave()
{
  //gDistillery.Visible = false;
}

/******************
*   BLAZH APPEARS
*******************/

function cBlazh_Appear() {
  cBlazh.Transparency = 100;
  BLAZH_VISIBLE = true;
  int trans = cBlazh.Transparency;
  while (trans > 0) {
    trans-=10;
    cBlazh.Transparency = trans;
    Wait(1);
  }
  cBlazh.Clickable = true;
  cBlazh.Transparency = 0;
  Wait(40);
  player.FaceCharacter(cBlazh);
  dDialogBlazh.Start();  
}


/***********
*   BUSH   *
************/

function oBush_PickUp()
{
  reachToPickUp();
  if(!BERRIES_PICKED_RED){
    DLG_StartInternalMonolog();
    DLG_DialogSayPlayer("( These seem like the ones Mara requested. )");
    DLG_EndDialog();
    //hBush.Enabled = false;
    oBush.Graphic = 338;
    addToPlayerInventory(iBasketBerriesBlazh);
    BERRIES_PICKED_RED = true;
    if(!BLAZH_VISIBLE)
      cBlazh_Appear();

  }
  else {
    DLG_StartInternalMonolog();
    DLG_DialogSayPlayer(" ( I've picked all the berries from this one. )");
    DLG_EndDialog();
  }

  
  LCM_clearFocus();
}


function oBush_Look()
{
  DLG_StartInternalMonolog();
  DLG_SetLeftEmotion(EmoSmile);
  DLG_DialogSayPlayer("( It's an evergreen berry bush! These are rare. )");
  DLG_EndDialog();
  LCM_clearFocus();
}

function oBush_Talk()
{
  MISC_defaultAction(2, eModeTalkto);
  LCM_clearFocus();
}

function oBush_UseInv()
{
  InventoryItem* item = iGUI_getLastChosenItem();
  
  switch(item){
      case iBobovacha:
      case iBabovacha:
      case iInvisovacha:
      case iBogovacha:
      case iDeafBobovacha:
      case iNoggenfoggerovacha:
      case iSmellyBobovacha:
      case iVertigoBobovacha:
      case iPickleBlazhovacha:
        DLG_StartInternalMonolog();
        DLG_SetLeftEmotion(EmoNeutral);
        DLG_DialogSayLeft("( Best to use it wisely. )");
        DLG_EndDialog();
        break;
      default:
        MISC_defaultAction(2, eModeUseinv);
        break;  
  }
  LCM_clearFocus();
}

function hBush_Mode9()
{
  oBush.RemoveTint();
}


/*************
*   BARREL   *
**************/
function oBarrel_Look()
{
  DLG_StartInternalMonolog();
  DLG_SetLeftEmotion(EmoNeutral);
  DLG_DialogSayPlayer("( That's an odd place for a barrel. )");
  DLG_EndDialog();
  LCM_clearFocus();
}

function oBarrel_PickUp()
{
  player.LockView(11);
  if(player.Loop == 1)
    player.Animate(1, 1, eOnce, eBlock, eForwards);
  else
    player.Animate(2, 1, eOnce, eBlock, eForwards);
  player.UnlockView();
  oBarrel.TweenPosition(1.5,  866, 645, eEaseInSineTween, eBlockTween);
  oBarrel.TweenPosition(2.0,  866, 1000, eEaseInSineTween, eBlockTween);
  aBarrelBreak.Play();
  RestoreWalkableArea(3);
  Wait(40);
  BARREL_BROKEN = true;
  DLG_StartInternalMonolog();
  DLG_SetLeftEmotion(EmoThink);
  DLG_DialogSayPlayer(" Oops. ");
  DLG_EndDialog();

  LCM_clearFocus();
}

function oBarrel_Talk()
{
  MISC_defaultAction(2, eModeTalkto);
  LCM_clearFocus();
}

function oBarrel_UseInv()
{
  MISC_defaultAction(2, eModeUseinv);
  LCM_clearFocus();
}

function hBarrel_Mode8()
{
  tintAndFocus(oBarrel);
}

function hBarrel_Mode9()
{
  oBarrel.RemoveTint();
}

/*************
*    DOOR    *
**************/
function oDoorClosed_Look()
{
  DLG_StartInternalMonolog();
  DLG_SetLeftEmotion(EmoSmor);
  DLG_DialogSayPlayer("( Seems quite sturdy... )");
  DLG_EndDialog();
  LCM_clearFocus();
}

function oDoorClosed_PickUp()
{
  DLG_StartInternalMonolog();
  DLG_SetLeftEmotion(EmoSmor);
  DLG_DialogSayPlayer("( Nope, won't budge. )");
  DLG_EndDialog();
  LCM_clearFocus();
}

function oDoorClosed_Talk()
{
  DLG_StartInternalMonolog();
  DLG_SetLeftEmotion(EmoThink);
  DLG_DialogSayPlayer("Open Sesame!");
  DLG_DialogSayPlayer("Alohomora!");
  DLG_DialogSayPlayer("Mellon?");
  DLG_SetLeftEmotion(EmoSmor);
  DLG_DialogSayPlayer("( No use... )");
  DLG_EndDialog();
  LCM_clearFocus();
}

function oDoorClosed_UseInv()
{
  InventoryItem* item = iGUI_getLastChosenItem();

  switch(item){
      case iBobovacha:
      case iBabovacha:
      case iInvisovacha:
      case iBogovacha:
      case iDeafBobovacha:
      case iNoggenfoggerovacha:
      case iSmellyBobovacha:
      case iVertigoBobovacha:
      case iPickleBlazhovacha:
        DLG_StartInternalMonolog();
        DLG_SetLeftEmotion(EmoNeutral);
        DLG_DialogSayLeft("( Best to use it wisely. )");
        DLG_EndDialog();
        return;
        break;
      default:
        MISC_defaultAction(2, eModeUseinv);
        break;  
  }

  if(item == iKey){
    player.LockView(11);
    if(player.Loop == 1)
      player.Animate(1, 1, eOnce, eBlock, eForwards);
    else
      player.Animate(2, 1, eOnce, eBlock, eForwards);
    player.UnlockView();
    aUnlocking.Play();
    Wait(80);
    oDoorClosed.Visible = false;
    oDoorClosed.Clickable = false;
    oDoorOpen.Visible = true;
    oDoorOpen.Clickable = true;
    RestoreWalkableArea(4);
    DOOR_OPEN = true;
    player.LoseInventory(iKey);
    DLG_StartDialogWith(cBlazh);
    DLG_SetRightEmotion(EmoNeutral);
    DLG_SetLeftEmotion(EmoNeutral);
    DLG_DialogSayRight("Well done.");
    DLG_DialogSayRight("Wait.");
    DLG_SetLeftEmotion(EmoAngry);
    DLG_DialogSayPlayer("There's another gate?!");
    DLG_SetLeftEmotion(EmoThink);
    DLG_SetRightEmotion(EmoNeutral);
    DLG_DialogSayRight("Naturally.");
    DLG_DialogSayRight("What do you take me for?");
	  DLG_DialogSayRight("Security is a must.");
	  DLG_DialogSayRight("And without further ado.");
    DLG_SetLeftEmotion(EmoSmile);
    DLG_DialogSayRight("I present to you the second key.");
    DLG_EndDialog();
    Wait(80);
    DLG_StartDialogWith(cBlazh);
    DLG_SetRightEmotion(EmoThink);
    DLG_DialogSayRight(". . .");
    DLG_DialogSayRight("Um...");
    DLG_DialogSayRight("Where-");
    DLG_SetLeftEmotion(EmoAngry);
    DLG_DialogSayPlayer("Don't even say it!");
    DLG_SetLeftEmotion(EmoSmor);
    DLG_DialogSayRight("I'm going back to Mara to ask for it.");
    DLG_SetRightEmotion(EmoSmile);
    DLG_DialogSayRight("Haha, knew you were a clever girl.");
    DLG_SetLeftEmotion(EmoSmor);
    DLG_DialogSayPlayer(". . .");	   
    DLG_EndDialog();
  } else MISC_defaultAction(2, eModeUseinv);
  
  LCM_clearFocus();
}

function oDoorOpen_Look()
{
  DLG_StartInternalMonolog();
  DLG_SetLeftEmotion(EmoSmor);
  DLG_DialogSayPlayer("( Like opening a matryoshka... )");
  DLG_EndDialog();
  LCM_clearFocus();
}

function oDoorOpen_PickUp()
{
  DLG_StartInternalMonolog();
  DLG_SetLeftEmotion(EmoSmor);
  DLG_DialogSayPlayer("( I worked hard to get it open. )");
  DLG_EndDialog();
  LCM_clearFocus();
}

function oDoorOpen_Talk()
{
  MISC_defaultAction(2, eModeTalkto);
  LCM_clearFocus();
}

function oDoorOpen_UseInv()
{
  InventoryItem* item = iGUI_getLastChosenItem();
  
  switch(item){
      case iBobovacha:
      case iBabovacha:
      case iInvisovacha:
      case iBogovacha:
      case iDeafBobovacha:
      case iNoggenfoggerovacha:
      case iSmellyBobovacha:
      case iVertigoBobovacha:
      case iPickleBlazhovacha:
        DLG_StartInternalMonolog();
        DLG_SetLeftEmotion(EmoNeutral);
        DLG_DialogSayLeft("( Best to use it wisely. )");
        DLG_EndDialog();
        break;
      default:
        MISC_defaultAction(2, eModeUseinv);
        break;  
  }
  LCM_clearFocus();
}

/***************************
*    DISTILLERY BUTTONS    *
****************************/
function oLowHeat_PickUp()
{
  DST_runLow();
  
  LCM_clearFocus();
}

function oLowHeat_Look()
{
  DLG_StartInternalMonolog();
  DLG_SetLeftEmotion(EmoNeutral);
  DLG_DialogSayPlayer("( These look like the temperature settings. )");
  DLG_EndDialog();
  LCM_clearFocus();
}

function oLowHeat_Talk()
{
  MISC_defaultAction(2, eModeTalkto);
  LCM_clearFocus();
}

function oLowHeat_UseInv()
{
  InventoryItem* item = iGUI_getLastChosenItem();
  
  switch(item){
      case iBobovacha:
      case iBabovacha:
      case iInvisovacha:
      case iBogovacha:
      case iDeafBobovacha:
      case iNoggenfoggerovacha:
      case iSmellyBobovacha:
      case iVertigoBobovacha:
      case iPickleBlazhovacha:
        DLG_StartInternalMonolog();
        DLG_SetLeftEmotion(EmoNeutral);
        DLG_DialogSayLeft("( Best to use it wisely. )");
        DLG_EndDialog();
        return;
        break;  
      default:
        MISC_defaultAction(2, eModeTalkto);
  }
  
  LCM_clearFocus();
}

function oMidHeat_PickUp()
{
  DST_runMid();
  LCM_clearFocus();
}

function oMidHeat_Look()
{
  DLG_StartInternalMonolog();
  DLG_SetLeftEmotion(EmoNeutral);
  DLG_DialogSayPlayer("( These look like the temperature settings. )");
  DLG_EndDialog();
  LCM_clearFocus();
}

function oMidHeat_Talk()
{
  MISC_defaultAction(2, eModeTalkto);
  LCM_clearFocus();
}

function oMidHeat_UseInv()
{
  InventoryItem* item = iGUI_getLastChosenItem();
  
  switch(item){
      case iBobovacha:
      case iBabovacha:
      case iInvisovacha:
      case iBogovacha:
      case iDeafBobovacha:
      case iNoggenfoggerovacha:
      case iSmellyBobovacha:
      case iVertigoBobovacha:
      case iPickleBlazhovacha:
        DLG_StartInternalMonolog();
        DLG_SetLeftEmotion(EmoNeutral);
        DLG_DialogSayLeft("( Best to use it wisely. )");
        DLG_EndDialog();
        return;
        break;  
      default:
        MISC_defaultAction(2, eModeTalkto);
  }
  
  LCM_clearFocus();
}

function oHighHeat_PickUp()
{
  DST_runHigh();
  LCM_clearFocus();
}

function oHighHeat_Look()
{
  DLG_StartInternalMonolog();
  DLG_SetLeftEmotion(EmoNeutral);
  DLG_DialogSayPlayer("( These look like the temperature settings. )");
  DLG_EndDialog();
  LCM_clearFocus();
}

function oHighHeat_Talk()
{
  MISC_defaultAction(2, eModeTalkto);
  LCM_clearFocus();
}

function oHighHeat_UseInv()
{
  InventoryItem* item = iGUI_getLastChosenItem();
  
  switch(item){
      case iBobovacha:
      case iBabovacha:
      case iInvisovacha:
      case iBogovacha:
      case iDeafBobovacha:
      case iNoggenfoggerovacha:
      case iSmellyBobovacha:
      case iVertigoBobovacha:
      case iPickleBlazhovacha:
        DLG_StartInternalMonolog();
        DLG_SetLeftEmotion(EmoNeutral);
        DLG_DialogSayLeft("( Best to use it wisely. )");
        DLG_EndDialog();
        return;
        break;  
      default:
        MISC_defaultAction(2, eModeTalkto);
  }
  
  LCM_clearFocus();
}

/************************
*    DISTILLERY TAP     *
*************************/

function oTap_PickUp()
{
  if(checkForBobovacha()){
    addToPlayerInventory(pourBobovacha());
    DLG_StartInternalMonolog();
    DLG_SetLeftEmotion(EmoNeutral);
    DLG_DialogSayLeft("( This looks quite potent. )");
    DLG_EndDialog();
  }else{
    DLG_StartInternalMonolog();
    DLG_SetLeftEmotion(EmoNeutral);
    DLG_DialogSayPlayer("( Nothing to pour. )");
    DLG_EndDialog();    
  }
  
  LCM_clearFocus();
}

function oTap_Look()
{
  DLG_StartInternalMonolog();
  DLG_SetLeftEmotion(EmoSmile);
  DLG_DialogSayPlayer("( That is a cute glass faucet. )");
  DLG_EndDialog();
  LCM_clearFocus();
}

function oTap_Talk()
{
  MISC_defaultAction(2, eModeTalkto);
  LCM_clearFocus();
}

function oTap_UseInv()
{
  InventoryItem* item = iGUI_getLastChosenItem();
  
  switch(item){
      case iBobovacha:
      case iBabovacha:
      case iInvisovacha:
      case iBogovacha:
      case iDeafBobovacha:
      case iNoggenfoggerovacha:
      case iSmellyBobovacha:
      case iVertigoBobovacha:
      case iPickleBlazhovacha:
        DLG_StartInternalMonolog();
        DLG_SetLeftEmotion(EmoNeutral);
        DLG_DialogSayLeft("( Best to use it wisely. )");
        DLG_EndDialog();
        return;
        break;  
  }
  LCM_clearFocus();
}

/************************
*    DISTILLERY IN      *
*************************/
function oIn_PickUp()
{
  DLG_StartInternalMonolog();
  DLG_SetLeftEmotion(EmoNeutral);
  DLG_DialogSayPlayer("( This is presumably where stuff goes in. )");
  DLG_EndDialog();
  LCM_clearFocus();
}

function oIn_Look()
{
  DLG_StartInternalMonolog();
  DLG_SetLeftEmotion(EmoNeutral);
  DLG_DialogSayPlayer("( A simple funnel. )");
  DLG_EndDialog();
  LCM_clearFocus();
}

function oIn_Talk()
{
  MISC_defaultAction(2, eModeTalkto);
  LCM_clearFocus();
}

function oIn_UseInv()
{
  InventoryItem* item = iGUI_getLastChosenItem();
  
  switch(item){
      case iBobovacha:
      case iBabovacha:
      case iInvisovacha:
      case iBogovacha:
      case iDeafBobovacha:
      case iNoggenfoggerovacha:
      case iSmellyBobovacha:
      case iVertigoBobovacha:
      case iPickleBlazhovacha:
        DLG_StartInternalMonolog();
        DLG_SetLeftEmotion(EmoNeutral);
        DLG_DialogSayLeft("( I think double-distilled would be overkill. )");
        DLG_EndDialog();
        return;
        break;  
  }
  
  if(item == iBasketBerriesBlazh || item == iBasketBerriesLake) {
      if(item == iBasketBerriesBlazh) {
          if(!DST_getBerriesWereOnceIn()) {
            DLG_StartInternalMonolog();
            DLG_DialogSayPlayer("( That should do. )");
            DLG_EndDialog();
          }
          else {
            DLG_StartInternalMonolog();
            DLG_DialogSayPlayer("( Here we go again.... )");
            DLG_EndDialog();         
          }
          player.LoseInventory(iBasketBerriesBlazh);
          redBerriesIn();
      }
      else if(item == iBasketBerriesLake) {
          if(!DST_getBerriesWereOnceIn()) {
            DLG_StartInternalMonolog();
            DLG_DialogSayPlayer("( Nice. )");
            DLG_EndDialog();
          }
          else {
            DLG_StartInternalMonolog();
            DLG_DialogSayPlayer("( This is probably enough for a few more tries.... )");
            DLG_EndDialog();         
          }
          player.LoseInventory(iBasketBerriesLake);
          blueBerriesIn();
      }
  }
  else {
    DLG_StartInternalMonolog();
    DLG_DialogSayPlayer("( That would just be mean... )");
    DLG_EndDialog();
  }
  
  if(getRedBerriesIn() && getBlueBerriesIn()){
    DST_berriesIn();
  }
  
  LCM_clearFocus();
}

/************************
*  DISTILLERY SLIDERS   *
*************************/

function oSliders_PickUp()
{
  gDSTSliders.Visible = true;
}

function oSliders_Look()
{
  DLG_StartInternalMonolog();
  DLG_SetLeftEmotion(EmoThink);
  DLG_DialogSayPlayer(String.Format("It's set to %d percent red berries, and %d percent water.", DST_getRedBerriesPercent(), DST_getWaterPercent()));
  DLG_EndDialog();
  LCM_clearFocus();
}

function oSliders_Talk()
{
  MISC_defaultAction(2, eModeTalkto);
  LCM_clearFocus();
}

function oSliders_UseInv()
{
  InventoryItem* item = iGUI_getLastChosenItem();
  
  switch(item){
      case iBobovacha:
      case iBabovacha:
      case iInvisovacha:
      case iBogovacha:
      case iDeafBobovacha:
      case iNoggenfoggerovacha:
      case iSmellyBobovacha:
      case iVertigoBobovacha:
      case iPickleBlazhovacha:
        DLG_StartInternalMonolog();
        DLG_SetLeftEmotion(EmoNeutral);
        DLG_DialogSayLeft("( Best to use it wisely. )");
        DLG_EndDialog();
        return;
        break;
      default:
        MISC_defaultAction(2, eModeUseinv);
  }
  LCM_clearFocus();
}

/************************
*        JOURNAl        *
*************************/

function oJournal_Interact()
{
  
  LCM_clearFocus();
}

function oJournal_PickUp()
{
  gJournal.Visible = true;
  int trans = gJournal.Transparency;
  while (trans > 0) {
    trans-=10;
    gJournal.Transparency = trans;
    Wait(1);
  }
  while(1) { if(WaitMouseKey(1)) break; else continue; }
  trans = gJournal.Transparency;
  while (trans < 100) {
    trans+=10;
    gTutorial.Transparency = trans;
    Wait(1);
  }
  gJournal.Visible = false;
  LCM_clearFocus();
}

function oJournal_Talk()
{
  DLG_StartInternalMonolog();
  DLG_DialogSayPlayer("( While it wouldn't be too far-fetched for it to talk at this point, I don't think it will. )");
  DLG_EndDialog();
  LCM_clearFocus();
}

function oJournal_Look()
{
  // TODO: implement journal showing
  gJournal.Visible = true;
  int trans = gJournal.Transparency;
  while (trans > 0) {
    trans-=10;
    gJournal.Transparency = trans;
    Wait(1);
  }
  while(1) { if(WaitMouseKey(1)) break; else continue; }
  trans = gJournal.Transparency;
  while (trans < 100) {
    trans+=10;
    gTutorial.Transparency = trans;
    Wait(1);
  }
  gJournal.Visible = false;
  LCM_clearFocus();

}

function oJournal_UseInv() {
  InventoryItem* item = iGUI_getLastChosenItem();
  
  switch(item){
      case iBobovacha:
      case iBabovacha:
      case iInvisovacha:
      case iBogovacha:
      case iDeafBobovacha:
      case iNoggenfoggerovacha:
      case iSmellyBobovacha:
      case iVertigoBobovacha:
      case iPickleBlazhovacha:
        DLG_StartInternalMonolog();
        DLG_SetLeftEmotion(EmoNeutral);
        DLG_DialogSayLeft("( Best to use it wisely. )");
        DLG_EndDialog();
        break;
      default:
        MISC_defaultAction(2, eModeUseinv);
        break;  
  }
  LCM_clearFocus();
}

function hJournal_Mode8()
{
  tintAndFocus(oJournal);
}

function hJournal_Mode9()
{
  oJournal.RemoveTint();
}