// new module header

import function MB_newGame();
import function MB_loadGame();
import function MB_settings();
import function MB_closeSettings();
import function MB_credits();
import function MB_exit();
import function MB_save();
import function MB_saveTab();
import function MB_loadTab();
import function MB_toMain();
import function MB_toMainInstant();

import function MB_getSaveToggled();
import function MB_getLoadToggled();

import function MB_toggleFullscreen();
//import function MB_toggleVSync();
import function MB_saveLoadSlider();