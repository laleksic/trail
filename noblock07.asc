// Main script for module 'NoBlock'

int action1;
int loctype1;
int locvalue1;
int dodo;
int xdest;
int ydest;
int mbutt;
int cancel=1;
int nogo=1;
int moveto=1;
int clicked;
int onlywalk=1;
String nogotext;


int hotx[6000];
int hoty[6000];

int charx[300];
int chary[300];

int objx[6000];
int objy[6000];


static function NoBlock::GetHotXY(int x, int y){

 Hotspot *myhotspot = Hotspot.GetAtScreenXY(x, y);
 //int myval = 0;
 if (myhotspot!=null) return myhotspot.ID;

}

static function NoBlock::GetCharXY(int x, int y){

 Character *mychar = Character.GetAtScreenXY(x, y);
 //int myval = 0;
 if (mychar!=null) return mychar.ID;

}

static function NoBlock::GetObjXY(int x, int y){

 Object *myobj = Object.GetAtScreenXY(x, y);
 //int myval = 0;
 if (myobj!=null) return myobj.ID;
  
}


static function NoBlock::UseHotspotWalkto(){

 int roomis=player.Room;

 int a=1;
 while (a < 30) {
  int b=a+(roomis*30);
  hotx[b]=hotspot[a].WalkToX;
  hoty[b]=hotspot[a].WalkToY;
  a++;
 }

}


static function NoBlock::Do(int action, int loctype, int locvalue){

 action1=action;
 loctype1=loctype;
 locvalue1=locvalue;

 if (loctype==eLocationHotspot) {
 xdest=hotx[(player.Room*30)+locvalue];
 ydest=hoty[(player.Room*30)+locvalue];
 }

 if (loctype==eLocationCharacter) {
 xdest=charx[locvalue];
 ydest=chary[locvalue];
 }

 if (loctype==eLocationObject) {
 xdest=objx[(player.Room*30)+locvalue];
 ydest=objy[(player.Room*30)+locvalue];
 }

 if ((xdest>0)||(ydest>0)){
  player.Walk(xdest, ydest, eNoBlock, eWalkableAreas);
 }
 mbutt=1;
}

static function NoBlock::Abort() {
action1=0;
}

static function NoBlock::GetAction() {
return action1;
}

static function NoBlock::CancelOnClick(int yesno) {
 if (yesno==0) {cancel=0;} else cancel=1;
}

static function NoBlock::DisplayNoGoText(int yesno) {
 if (yesno==0) {nogo=0;} else nogo=1;
}

static function NoBlock::SetNoGoText(String typehere){
  //StrCopy(nogotext, typehere);
  nogotext = typehere;
}

static function NoBlock::SetHotspotPoint(int room, int number, int x, int y){
 hotx[(room*30)+number]=x;
 hoty[(room*30)+number]=y;
}

static function NoBlock::SetObjectPoint(int room, int number, int x, int y){
 objx[(room*30)+number]=x;
 objy[(room*30)+number]=y;
}

static function NoBlock::SetCharacterPoint(int number, int x, int y){
 charx[number]=x;
 chary[number]=y;
}

function RunInter(){

 if (loctype1==eLocationHotspot) {
  hotspot[locvalue1].RunInteraction(action1);
 }

 if (loctype1==eLocationObject) {
  object[locvalue1].RunInteraction(action1);
 }

 if (loctype1==eLocationCharacter) {
  character[locvalue1].RunInteraction(action1);
 }

}

static function NoBlock::MoveToWalkToPoints(int yesno) {
 moveto=yesno;
}

static function NoBlock::GetLocX(int room, int kind, int number){
 if (kind==eLocationHotspot) {return hotx[(room*30)+number];}
 if (kind==eLocationObject) {return objx[(room*30)+number];}
 if (kind==eLocationCharacter) {return charx[number];}
}

static function NoBlock::GetLocY(int room, int kind, int number){
 if (kind==eLocationHotspot) {return hoty[(room*30)+number];}
 if (kind==eLocationObject) {return objy[(room*30)+number];}
 if (kind==eLocationCharacter) {return chary[number];}
}

static function NoBlock::MoveOnlyOnWalkCursor(int yesno){
 onlywalk=yesno;
}


//end of custom functions


function game_start() {

 //StrCopy(nogotext, "Whoops I didn't get there so I could not do the stuff you wanted!");
  nogotext = "Whoops I didn't get there so I could not do the stuff you wanted!";
}


function repeatedly_execute_always(){

 if (IsInterfaceEnabled()==0) {action1=0;}

}


function repeatedly_execute(){


 if (action1 >0)
 {
 if (cancel==1) {
  if ((mouse.IsButtonDown(eMouseLeft)==1)&&(mbutt==0)) {action1=0;} //cancel when clicked
  if (mouse.IsButtonDown(eMouseLeft)==0) {mbutt=0;}
 }

 if ((xdest>0)||(ydest>0)) {
  if (player.Moving==0) {
    if ((player.x==xdest)&&(player.y==ydest)) {RunInter();} else {action1=0;if (nogo==1) {player.Say(nogotext);}}
   }
  } else {RunInter();}
 }

 //move to walkto points


int meh=0;
if (onlywalk==1) {meh=1;}
if (Mouse.Mode!=eModeWalkto) {meh++;}

 if ((mouse.IsButtonDown(eMouseLeft)==1) && (GetLocationType(mouse.x, mouse.y)==eLocationHotspot)&&(moveto==1)&&(clicked==0)&&(IsInterfaceEnabled()==1)&&(IsGamePaused()==0)&&(meh<2)){

  int a=0;
  int myhot=0;
  while (a < 30) {
  if (Hotspot.GetAtScreenXY(mouse.x, mouse.y)==hotspot[a]) {myhot=a;}
  a++;
  }  

  myhot=myhot+(player.Room*30);
  if  ((hotx[myhot]>0)||(hoty[myhot]>0)) {
   player.Walk(hotx[myhot], hoty[myhot], eNoBlock, eWalkableAreas); clicked=5;
  }
 }



 if ((mouse.IsButtonDown(eMouseLeft)==1) && (GetLocationType(mouse.x, mouse.y)==eLocationObject)&&(moveto==1)&&(clicked==0)&&(IsInterfaceEnabled()==1)&&(IsGamePaused()==0)){

  int a=0;
  int myobj=0;
  while (a < 20) {
  if (Object.GetAtScreenXY(mouse.x, mouse.y)==object[a]) {myobj=a;}
  a++;
  }  

  myobj=myobj+(player.Room*30);
  if  ((objx[myobj]>0)||(objy[myobj]>0)) {
   player.Walk(objx[myobj], objy[myobj], eNoBlock, eWalkableAreas); clicked=5;
  }
 }



 if ((mouse.IsButtonDown(eMouseLeft)==1) && (GetLocationType(mouse.x, mouse.y)==eLocationCharacter)&&(moveto==1)&&(clicked==0)&&(IsInterfaceEnabled()==1)&&(IsGamePaused()==0)){

  int a=0;
  int chara=0;
  while (a < Game.CharacterCount) {
  if (Character.GetAtScreenXY(mouse.x, mouse.y)==character[a]) {chara=a;}
  a++;
  }  


  if  ((charx[chara]>0)||(chary[chara]>0)) {
   player.Walk(charx[chara], chary[chara], eNoBlock, eWalkableAreas); clicked=5;
  }
 }



 if ((mouse.IsButtonDown(eMouseLeft)==0)&&(clicked>0)) {clicked--;}

}
