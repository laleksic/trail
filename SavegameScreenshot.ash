// Script header for module 'Savegames with screenshots'
// Author: Andrew MacCormack (SSH)
//   Please use the PM function on the AGS forums to contact
//   me about problems with this module
// 
// Abstract: Provides a savegames with screenshots GUI
// Dependencies:
//
//   AGS 2.71beta6 or later
//   YesNo module and GUE
//   Savegame GUE
//
// Functions:
//   sgs.Save()			Start GUI in save mode
//   sgs.Load()			Start GUI in load mode
//
//   The other functions should be called only from the
//   SAVELOAD GUI.
//
//   sgs.guiclose();
//   sgs.guiup();
//   sgs.guidown();
//   sgs.guiok();
//   sgs.guiss(Button *bpressed);
//
//   The functions in the accompanying GUE show how these
//   should be called.
//   
// Configuration:
//   The GUI may be configured using the #defines at the
//   bottom of the script header. The simplest configuration
//   is changing the colours of the GUI labels when selected
//   or not selected. Copy down the colour numbers from the
//   palette and substitute them below.
//
// SGS_SLOTS_ONSCREEN must match the number of buttons and 
// text labels on the SAVELOAD GUI that are to be used for
// displaying screenshots and descriptions.
//
// SGS_MAX_SLOT sets the total number of savegame slots that
// you wish to be available. If this is greater than
// SGS_SLOTS_ONSCREEN then the dialog will allow scrolling.
//
// DOUBLECLICK_DELAY sets the delay allowed between clicks
// of a double-click in game cycles.
//
// SGS_FLASH_PERIOD sets how quickly the cursor flashes when
// typing a slot description
//
// SGS_BUTTON_OFFSET is the GUI object number of the first
// screenshot button. All the screenshot buttons must have
// contiguous object numbers, so be careful when making 
// your GUI.
//
// SGS_LABEL_OFFSET is the GUI object number of the first
// screenshot label. All the screenshot labels must have
// contiguous object numbers, so be careful when making
// your GUI. In addition, the order of the labels should
// match the order of the buttons.
//
//   Also, part of the configutation is the width and height
//   of the buttons (which must all be the same) and the 
//	 width and font of the labels (which must also be the 
//   same). There are picked up automatically from the GUI
//   objects named goSaveLoadLabel and goSaveLoad_one.
//
//   The GUE accompanying this module shows how the buttons
//   should be configured, with on-click functions which call
//   functions within this module.
//
// Tutorial:
//   To integrate this saveload GUI with a game that just 
//   uses the default save/load GUIs is quite easy. Just 
//   import the YesNo GUE and SCM files, along with this 
//   script module and GUE and make sure the YesNo module
//   is listed before this one in the module manager.
//   Then, edit the global script and replace both 
//   occurances of "RestoreGameDialog()" with "sgs.Load()"
//   and both "SaveGameDialog()" with "sgs.Save()";
//
// Caveats:
//   Only tested in 16bit graphics mode
//   Versions of AGS prior to 2.71b5 had a bug in GUE import
//   
// Revision History:
//
//    May 04: First used in Awakening of the Sphinx demo
//    Jul 04: Tutorial published in AGS ezine
//    Feb 05: Reworked to AGS 2.7 for February MAGS
//  9 Sep 05: Released as standalone module
// 17 Sep 05: Updated for AGS 2.71 beta 6
//
// Licence:
//
//   Savegames with Screenshots AGS script module
//   Copyright (C) 2005 Andrew MacCormack
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation; either
//   version 2.1 of the License, or (at your option) any later version.
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with this library; if not, write to the Free Software
//   Foundation, Inc, 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//=========================================================


struct sgs {
  // Functions to be called from SAVELOAD GUI
  import static function guiclose();
  import static function guiup();
  import static function guidown();
  import static function guiok();
  import static function guiss(Button *bpressed);
  // Functions to trigger loads and saves
  import static function Save();
  import static function Load();
};


#define DOUBLECLICK_DELAY 16
// set label colours for when selected or not 
#define SGS_LABEL_COLOUR  42058
#define SGS_SELECT_COLOUR 58962
// For scrolling version
#define SGS_MAX_SLOT        100
// Number of slots show on GUI at once
#define SGS_SLOTS_ONSCREEN    4
// Number of first slot label
#define SGS_LABEL_OFFSET      8
// Number of first slot button
#define SGS_BUTTON_OFFSET     0
// Cursor flash period
#define SGS_FLASH_PERIOD     20

