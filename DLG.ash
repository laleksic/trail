//=========================
//===========Customizations
//=========================

// Font for displaying dialog options
#define DIALOG_OPTIONS_FONT eFontfntAnalecta14p

// Should we enumerate (1. 2. 3. etc) dialog options?
//#define DIALOG_OPTIONS_ENUMERATE

// How should we align dialog options?
#define DIALOG_OPTIONS_ALIGN eAlignCentre

/*
LINE_WIDTH is the maximum line length in characters
for the txtSpeech in gDialog (this is where the dialog txt
is displayed).

LINE_COUNT is the maximum number of lines txtSpeech can display
before starting to scroll automatically.

Both need to be calibrated by hand depending on the font used
for txtSpeech (the font is set in the IDE).
*/
// good values for analecta 12p
//#define LINE_WIDTH 60
//#define LINE_COUNT 9

// good values for analecta 14p
//#define LINE_WIDTH 48
//#define LINE_COUNT 8

//good values for 
#define LINE_WIDTH 50
#define LINE_COUNT 8

// Set to sprite index of empty portrait bg
#define BG_EMPTY_SPRITE_IDX 78

//=========================
//=====EMOTIONS(sissy stuff)
//=========================
// This is also the order of sprites in a character's speech view!!!
enum Emotion {
	EmoNeutral = 0, 
	EmoSmug, 
	EmoSmile, 
	EmoSmor, 
	EmoThink, 
	EmoAngry
};

//=========================
//================INTERFACE
//=========================
// Documented in detail in the doc file, also see examples (TODO make examples :P).
import function DLG_StartDialog(Character* leftSpeaker,  Character* rightSpeaker,  Emotion leftEmotion = EmoNeutral,  Emotion rightEmotion = EmoNeutral);
import function DLG_StartDialogWith(Character* other, Emotion playerEmotion = EmoNeutral,  Emotion otherEmotion = EmoNeutral);
import function DLG_StartInternalMonolog();

import function DLG_EndDialog();

import function DLG_DialogSay(Character* speaker, String speech);
import function DLG_DialogSayLeft(String speech);
import function DLG_DialogSayRight(String speech);
import function DLG_DialogSayPlayer(String speech);

import function DLG_SetLeftSpeaker(Character* speaker);
import function DLG_SetRightSpeaker(Character* speaker);

import function DLG_SetLeftEmotion(Emotion emo);
import function DLG_SetRightEmotion(Emotion emo);

import function dialog_setIsActive(bool value);
import function dialog_getIsActive();