import function DST_setRedBerries(int val);
import function DST_setBlueBerries(int val);
import function DST_setWaterPercent(int val);

import function DST_getRedBerriesPercent();
import function DST_getBlueBerriesPercent();
import function DST_getWaterPercent();
import function DST_getBerriesWereOnceIn();
import function DST_berriesIn();
import function DST_outOfBerries();

import function DST_runLow();
import function DST_runMid();
import function DST_runHigh();

import function getRedBerriesIn();
import function getBlueBerriesIn();
import function redBerriesIn();
import function blueBerriesIn();

import InventoryItem* pourBobovacha();
import function checkForBobovacha();