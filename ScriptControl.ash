
enum GState {
  
  gstNormal, 
  gstNonInteractive, 
  gstDialogue, 
  gstJournal, 
  gstInventory,
  
  };

//Left Click Menu Control
import function setDisableLCM(bool value);
import function getDisableLCM();
import function setDisableWalking(bool value);
import function getDisableWalking();
import function LCM_getMovingToFocus();
import function LCM_setMovingToFocus(bool value);
import function LCM_getCombining();
import function LCM_setCombining(bool value);

//Room Transtition Control
import function setDisableRT(bool value);
import function getDisableRT();
import function setClickedRT(bool value);
import function getClickedRT();
import function setCurrentRoom(int room);
import function getCurrentRoom();

enum RT_Rooms {
  FOREST_BEAR = 1, 
  FOREST_HUT = 2, 
  FOREST_START = 3, 
  LAKE = 4, 
  BLAZH_H = 5
};

//Inventory
import function setDisableInventory(bool value);
import function getDisableInventory();
import InventoryItem* getCombineItem();
import function setCombineItem(InventoryItem* item);

//Game state control
import function setCurrState(GState value);
import function getCurrState();
import function getStateChanged();
import function setStateChanged(bool value);

//Pause Menu
import function SC_setPaused(bool value);
import function SC_getPaused();
import function SC_togglePaused();
import function SC_setPauseDisabled(bool value);
import function SC_getPauseDisabled();