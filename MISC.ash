/*

MISC module (not yet categorized

*/

import function MISC_defaultAction(int interactable,  int modeOfInteraction);
import function MISC_centerAtMouseX(int w);
import function MISC_centerAtMouseY(int h);

#define NORMAL_WALK_SPEED 12
#define RUNNING_SPEED 24

#define INVENTORY_GRAPHIC 32
#define COMBINE_GRAPHIC 119
#define INVENTORY_HOVER_GRAPHIC 118
#define COMBINE_HOVER_GRAPHIC 120
#define INVENTORY_PRESSED_GRAPHIC 113
#define COMBINE_PRESSED_GRAPHIC 121

import function addToPlayerInventory(InventoryItem* item);

import function GammaToBrightness(int gammaVal);

import function bendToPickUp();
import function reachToPickUp();

import function loadSettings();