/* quest api */

#define numQuests 4

#define MAX_QUEST_STATES 16

//#define NUM_QUEST_STATES MAX_QUEST_STATES * numQuests
//#define NUM_QUEST_STATES 16 * 3
#define NUM_QUEST_STATES 64

//length in chars of a single page in quest log
#define QUEST_LOG_PAGE_LENGTH 600

// Quest state flags
#define QUEST_HIDDEN    1 << 1  /* quest hidden from the player in this state */
#define QUEST_OPTIONAL  1 << 2  /* (UNUSED!) quest is optional in this state */
#define QUEST_COMPLETED 1 << 3  /* quest is completed in this state */
#define QUEST_FAILED    1 << 4  /* (UNUSED!) ...unsuccessfuly? (relevant iff QUEST_COMPLETED) */
#define QUEST_NOTIFY    1 << 5  /* trigger notification */
#define QUEST_NEW       1 << 6  /* first reveal (relevant iff QUEST_NOTIFY) */

#define MAX_LINES 2048
#define LINES_PER_PAGE 18

struct QuestState {
	String name;
	String desc;
	bool visited;
	int flags;
	
	int desc_lines;
	int desc_lsi[MAX_LINES];
	int desc_lei[MAX_LINES];
};

// stores the curr quest state for each quest
import int Quests[numQuests];

/* retreive quest state from here by doing
//		int sID = Quests[0] //idx for curr state for quest 0
//		QuestState state = QuestStates[sID]; // there we go! */
import QuestState QuestStates[NUM_QUEST_STATES];

import void QuestSetState(int qID, int sID);

import int GetStateIdx(int qID, int sID);
import int GetCurrentStateIdx(int qID);

import void QUEST_showCompletedQuests(bool show);
import bool QUEST_showingCompletedQuests();

import void QUESTPrevPage();
import void QUESTNextPage();