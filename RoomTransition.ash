/* This script checks if the player is hovering over
** a transition region (a region where they can click to
** go into another room), displays the adequate arrow as
** coursor, and performs the transition between rooms if
** the player chooses to click
*/

import function RT_setRooms(int currentRoom);
import function RT_getCurrRoom();
import function RT_getLastRoom();
import function RT_setDisable(bool value);
import function RT_goToRoom(int roomID);
import function RT_setClicked(int value);

enum RT_mouseState {
    Hovering, 
    NotHovering
};
 
enum RT_cursorGraphics {
    HoveringGraphic = 79, 
    NotHoveringGraphic = 5
};

enum RT_transitions {
    FS_FB = 1,
    FB_FS = 1, 
    FB_FH = 2, 
    FH_FB = 1, 
    FH_L  = 2, 
    L_FH  = 1, 
    L_B   = 2,
    BH_L  = 1, 
    BH_TE = 2
};