// Master GUI controler
// A most primitive method of inter module communication without circdeps

// LCM control
enum LCMMode {
	WorldInteraction, 
	ItemInteraction
};

import function LCM_show(LCMMode mode = WorldInteraction);
import function LCM_hide();
import function LCM_toggle(LCMMode mode = WorldInteraction);
import function LCM_doneShowing();
import function LCM_doneHiding();
import bool LCM_getShown();
import bool LCM_getShouldShow();
import bool LCM_getShouldHide();
import LCMMode LCM_getMode();
import bool LCM_isWaitingForItem();
import function LCM_setWaitingForItem(bool waiting);

// iGUI control
/*
	Inventory works in two modes depending on whether it's started from the iconbar
	or from the left-click menu.
	
	If started from the icon bar, when selecting an item, you get a left-click menu
	for interacting with the item.
	
	If started from the left-click menu, when selecting an item it is immediately
	"applied" to the object whose interaction started the aforementioned LCM.
*/
enum iGUIMode {
	StartedFromIconbar, 
	StartedFromLCM
};

import function iGUI_show(iGUIMode mode);
import function iGUI_hide();
import function iGUI_toggle(iGUIMode mode);
import function iGUI_doneShowing();
import function iGUI_doneHiding();
import bool iGUI_getShown();
import bool iGUI_getShouldShow();
import bool iGUI_getShouldHide();
import iGUIMode iGUI_getMode();
import InventoryItem* iGUI_getChosenItem();
import function iGUI_setChosenItem(InventoryItem* item);
import InventoryItem* iGUI_getLastChosenItem();

//QUEST (quest log) control
import function QUEST_show();
import function QUEST_hide();
import function QUEST_doneShowing();
import function QUEST_doneHiding();
import function QUEST_toggle();
import bool QUEST_getShown();
import bool QUEST_getShouldShow();
import bool QUEST_getShouldHide();

//Iconbar control
import function Iconbar_getShouldShow();
import function Iconbar_setShouldShow(bool value);

//Pause
import function togglePauseGUI();