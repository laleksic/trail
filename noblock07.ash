/* Header for NoBlock */

struct NoBlock {

import static function Do(int action, int loctype, int locvalue);
import static function Abort();
import static function GetAction();
import static function CancelOnClick(int yesno);
import static function DisplayNoGoText(int yesno);
import static function SetNoGoText(String typehere);
import static function UseHotspotWalkto();
import static function MoveToWalkToPoints(int yesno);
import static function MoveOnlyOnWalkCursor(int yesno);

import static function SetHotspotPoint(int room, int number, int x, int y);
import static function SetObjectPoint(int room, int number, int x, int y);
import static function SetCharacterPoint(int number, int x, int y);

import static function GetLocX(int room, int kind, int number);
import static function GetLocY(int room, int kind, int number);

import static function GetHotXY(int x, int y);
import static function GetCharXY(int x, int y);
import static function GetObjXY(int x, int y);
};


/*
NoBlock 0.7 by Bernie (email: webmaster@origamihero.com, website: www.origamihero.com)
--------------------------------------------------------------------------------------

NoBlock makes your characters head to walkto points non-blocking (custom or the AGS internal ones for
hotspots, if you wish). Upon reaching the destination (customizable), an interaction will be executed.


NoBlock.functions:

function Do(int action, int loctype, int locvalue);
-------------------------------------------------------
This one's the most important function. Define your action (you can also use those enumerated things
like eModeLookat), define the locationtype (you could use eLocationHotspot, etc), and then define
which location you want to trigger the interaction for (you could retrieve it with GetHotXY()).

If you want to use hotspot walkto points, call MakeDoUseHotspotWalkto() at a room start, otherwise
you need to set it with the Set(Hotspot/Character/Object)Point functions listed below.

If a location's walkto points are 0 or lower, the interaction will be triggered instantly.


function Abort();
-----------------------
Makes nothing happen when the player reaches his destination (after calling MakeDo()).


function GetAction();
---------------------------
Returns the current action (Use, Look, etc. You can compare with the enumerates once again.)


function CancelOnClick(int yesno); (1 by default)
--------------------------------------------------------
If the player clicks somewhere, the next interaction will be cancelled. Pass 0 to make it stop doing
that.

function DisplayNoGoText(int yesno);
------------------------------------------
Whether to display a text if the destination could not be reached.


function SetNoGoText(string typehere);
--------------------------------------------
Alter the text to display when failing to reach destination.


function UseHotspotWalkto();
----------------------------------
Call this in a room (room start) to use the walkto points from hotspots.


function MoveToWalkToPoints(int yesno);
---------------------------------------------
If passed as 1, the character will move to a location's walkto point when left-clicking on it. This
only works if automatic walking is off.


function MoveOnlyOnWalkCursor(int yesno);
-----------------------------------------------
If you're using the module's walkto location (MakeDoMoveToWalkToPoints()), this can modify it only
to walk to a location when the cursor is in walking mode.


function SetHotspotPoint(int room, int number, int x, int y);
-------------------------------------------------------------
Tell the module where a hotspot's walkto point is.


function SetObjectPoint(int room, int number, int x, int y);
------------------------------------------------------------
Tell the module where an object's walkto point is.


function SetCharacterPoint(int number, int x, int y);
-----------------------------------------------------
Tell the module where a character's walkto point is.


function GetLocX(int room, int kind, int number);
-------------------------------------------------
Retrieves a location's x walkto coordinate. Value 'room' will be ignored if retrieving a character's
x-value.


function GetLocY(int room, int kind, int number);
-------------------------------------------------
Retrieves a location's y walkto coordinate. Value 'room' will be ignored if retrieving a character's
y-value.


function GetHotXY(int x, int y), GetCharXY(int x, int y) and GetObjXY(int x, int y);
------------------------------------------------------------------------------------
Allow to retrieve a location's number. You could do this:
MakeDo(eModeInteract,eLocationHotspot, GetHotXY(mouse.x,mouse.y));


You can make 'dynamic' walk-to points by repeatedly setting, say, a character's walkto point to its
x/y-position in repeatedly_execute.

*/