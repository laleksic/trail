/*

All LCM stuff goes here

Bitte use LCM_* prefix as alternative to namespaces.

*/

enum Verb {
	VerbLook, 
	VerbTake, 
	VerbUse, 
	VerbTalk, 
  VerbCombine
};




/* Pointer to the interactable selected by the LCM
Only one of these is non-null at a given moment */
/* These are set when first clicking, and used to trigger
interaction when a button is chosen.

Why do this instead of
sending interaction to mouse.x/y?

Because objs/chars may move
and we don't want to chase them with the mouse. 
We want to save the chosen interactable at the first point LCM is opened, not
the point at mouse coords when the action is chosen!!!

Plus it can be used to make sure that clicking twice in
a row on the same interactable doesn't move the LCM,
rather it should hide it.*/
import Hotspot* hsInFocus;
import Object* oInFocus;
import Character* chInFocus;
import InventoryItem* itemInFocus;

enum Focus {
	NothingInFocus, 
	
	HotspotInFocus, 
	ObjectInFocus, 
	CharacterInFocus, 
	ItemInFocus
};

import Focus glWhatsInFocus;


import function LCM_chooseAction(Verb act);

import function LCM_setFocusToObject(Object* o);
import function LCM_setFocusToChar(Character* c);

import function LCM_clearFocus();