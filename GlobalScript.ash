/*

Global script

Put in here *ONLY* stuff that *MUST* be here by AGS law.

Even then, the stuff that *MUST* be here, shouldn't do *ANYTHING*
except call functions defined in other modules.

*/