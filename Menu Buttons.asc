function MB_newGame(){
    setCurrState(gstNormal);
    LCM_hide();
    RT_setRooms(0);
    player.ChangeRoom(STARTING_ROOM);
}

function MB_settings(){
    loadSettings();
    gSettings.Transparency = 100;
    int trans = gSettings.Transparency;
    while (trans > 0) {
      trans-=10;
      gSettings.Transparency = trans;
      Wait(1);
    }
    //player.ChangeRoom(299);
}

function MB_closeSettings(){
    int trans = gSettings.Transparency;
    while (trans < 100) {
      trans+=10;
      gSettings.Transparency = trans;
      Wait(1);
    }
    gSettings.Visible = false;
}

function MB_credits(){
    //player.ChangeRoom(302);
    gCredits.Visible = true;
    gCredits.Transparency = 100;
    int trans = gCredits.Transparency;
    while (trans > 0) {
      trans-=10;
      gCredits.Transparency = trans;
      Wait(1);
    }
    while(1) { if(WaitMouseKey(1)) break; else continue; }
    gCredits.BackgroundGraphic = 316;
    while(1) { if(WaitMouseKey(1)) break; else continue; }
    trans = gCredits.Transparency;
    while (trans < 100) {
      trans+=10;
      gCredits.Transparency = trans;
      Wait(1);
    }
    gCredits.Visible = false;
    gCredits.BackgroundGraphic = 315;
}

function MB_exit(){
  gDialogbox.BackgroundGraphic = 305;
  if(DialogBox("Are you sure you want to quit?"))
		QuitGame(false);
}

function MB_toMain(){
  gDialogbox.BackgroundGraphic = 286;
  if(DialogBox("Are you sure you want to exit to Main Menu?")){
    player.ChangeRoom(301);
  }
}

function MB_toMainInstant(){
  player.ChangeRoom(301);
}

bool saveToggled = false;
function MB_getSaveToggled(){return saveToggled;}
function MB_save(){
  if(!saveToggled){
    sgs.Save();
    saveToggled = true;
    sldSaveLoad.Value = 1;
  }
  else{
    sgs.guiclose();
    saveToggled = false;
    gSaveload.Visible = false;
  }
}

bool loadToggled = false;
function MB_getLoadToggled(){return loadToggled;}
function MB_loadGame(){
  if(!loadToggled){
    sgs.Load();
    loadToggled = true;
    sldSaveLoad.Value = 0;
  }
  else{
    sgs.guiclose();
    loadToggled = false;
  }
}

function MB_saveTab(){
  if(!saveToggled){
    sgs.Save();
    saveToggled = true;
    loadToggled = false;
    sldSaveLoad.Value = 1;
  }
  gSaveload.Visible = true;
}

function MB_loadTab(){
  if(!loadToggled){
    sgs.Load();
    loadToggled = true;
    saveToggled = false;
    sldSaveLoad.Value = 0;
  }
  gSaveload.Visible = true;
}

function MB_saveLoadSlider(){
  if(sldSaveLoad.Value == 0){
    MB_loadTab();
  }
  else{
    MB_saveTab();
  }
}



function MB_toggleFullscreen(){
  if(System.Windowed){
    System.Windowed = false;
    bToggleFullScreen.NormalGraphic = 200;
    bToggleFullScreen.PushedGraphic = 200;
    bToggleFullScreen.MouseOverGraphic = 200;
  }
  else{
    System.Windowed = true;
    bToggleFullScreen.NormalGraphic = 199;
    bToggleFullScreen.PushedGraphic = 199;
    bToggleFullScreen.MouseOverGraphic = 199;
  }
}

/*function MB_toggleVSync(){
  if(System.VSync){
    Display("True to false");
    System.VSync = false;
    bToggleVSync.NormalGraphic = 200;
    bToggleVSync.PushedGraphic = 200;
    bToggleVSync.MouseOverGraphic = 200;
  }
  else{
    Display("False to true");
    System.VSync = true;
    bToggleVSync.NormalGraphic = 199;
    bToggleVSync.PushedGraphic = 199;
    bToggleVSync.MouseOverGraphic = 199;
  }
}*/