// Script header for module 'YesNo'
// YesNo provides functions for a Yes/No style dialog box

import function AskYesNo(const string question);
import int YesNo;
import function YesNo_close() ;

